Rails.application.routes.draw do
  root 'home#index'  
  get  'home/index'
  get 'import_file', to: 'data#import_file'
end
