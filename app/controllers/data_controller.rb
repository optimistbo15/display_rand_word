class DataController < ApplicationController
  
  def import_file
    # First pattern
    # file_data = File.read("cardholder.txt").split
    # render json: { success: true, response: file_data.sample}

    # Second pattern
    file_data = File.read("random_display.txt").split
    render json: { success: true, response: file_data.shuffle.first}

    # Third pattern
    # file_data = File.read("cardholder.txt").split
    # render json: { success: true, response: file_data[rand(file_data.count)]}
  end
end
