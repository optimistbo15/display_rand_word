require "rails_helper"

RSpec.describe DataController, :type => :controller do
  describe "GET import_file" do
    it "has a 200 status code" do
      get :import_file
      expect(response.status).to eq(200)
    end
  end
end
